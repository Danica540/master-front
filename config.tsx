import 'process';

const config = {
    polly: parseInt("process.env.POLLY_RETRY_COUNT"),
    url: process.env.BACKEND_API_URL
};

export default config;
