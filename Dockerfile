FROM node:12

WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
COPY tsconfig.json ./
COPY webpack.config.js ./

RUN npm install

COPY src ./src
COPY index.html ./
COPY config.tsx ./

ENTRYPOINT [ "npm" ,"start" ]