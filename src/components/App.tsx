import { Component } from "react";
import React from 'react';
import { getMeasurments, insertMeasurment } from '../services/data.service'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { options } from './chartOptions'

interface Props {
}

interface State {
    points: any[],
    value: number,
    name: string
}

class App extends Component<Props, State>{

    constructor(props: Props) {
        super(props);
        this.state = {
            points: [],
            name: "",
            value: 0
        }
    }

    componentDidUpdate = () => {

    }

    componentDidMount = () => {
        setInterval(() => {
            getMeasurments().then(res => {
                this.setState({ points: this.transformMeasurmentsToPoints(res) })
                if (Highcharts.charts[0]) {
                    let now = new Date().getTime()
                    const oneMinute = 1 * 60 * 1000
                    Highcharts.charts[0].xAxis[0].setExtremes(now - 2 * oneMinute, now)
                }
            });
        }, 2000)
    }

    transformMeasurmentsToPoints(measurments: any[]): any[] {
        let array: any[] = []
        measurments.forEach(element => {
            array.push({
                x: element.Timestamp,
                y: element.Value,
                name: element.Name
            })
        });
        return array
    }

    onBtnClick = (event: any) => {
        insertMeasurment({
            name: this.state.name,
            value: this.state.value,
            timestamp: new Date().getTime()
        })
    }

    onValueChange = (event: any) => {
        let value = event.target.value
        if (!this.isNumeric(value)) {
            value = 0
        }
        this.setState({ value: value })
    }

    onNameChange = (event: any) => {
        let pointName = event.target.value
        if (!pointName) {
            pointName = `Test`
        }
        this.setState({ name: pointName })
    }

    isNumeric = (value: any) => {
        return /^-?\d+$/.test(value);
    }

    render() {
        return (
            <div className={`main`}>
                <h1 className={`text-primary`}>MONITOR METRICS</h1>
                <div className={`chart`}>
                    <HighchartsReact
                        highcharts={Highcharts}
                        allowChartUpdate={true}
                        options={options(this.state.points)}
                    />
                </div>
                <div>
                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input type="text" onChange={this.onNameChange} className="form-control" id="name" placeholder="Enter name" />
                    </div>
                    <br />
                    <div className="form-group">
                        <label htmlFor="value">Value</label>
                        <input type="text" onChange={this.onValueChange} className="form-control" id="value" placeholder="Enter value" />
                    </div>
                    <br />
                    <button type="button" onClick={this.onBtnClick} className="btn btn-primary">Submit</button>
                </div>
            </div>
        )
    }
}


export default App;